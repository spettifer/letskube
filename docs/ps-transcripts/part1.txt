﻿**********************
Windows PowerShell transcript start
Start time: 20190307165722
Username: GLSHUK\pettifers
RunAs User: GLSHUK\pettifers
Configuration Name: 
Machine: DEVSP-LAPTOP (Microsoft Windows NT 10.0.17763.0)
Host Application: C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe
Process ID: 5612
PSVersion: 5.1.17763.316
PSEdition: Desktop
PSCompatibleVersions: 1.0, 2.0, 3.0, 4.0, 5.0, 5.1.17763.316
BuildVersion: 10.0.17763.316
CLRVersion: 4.0.30319.42000
WSManStackVersion: 3.0
PSRemotingProtocolVersion: 2.3
SerializationVersion: 1.1.0.1
**********************
Transcript started, output file is C:\_git\Pluralsight\Kubernetes Intro\ps-transcripts\part1.txt
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> az acr list --resource-group letskube-rg --query "[].{acrLoginServer:loginServer}" --output table
AcrLoginServer
------------------------
letskubeacrsp.azurecr.io
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl apply -f.\letskubedeploy.yml
deployment.apps "letskube-deployment" created
service "letskube-service" created
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get service --watch
NAME               TYPE           CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
kubernetes         ClusterIP      10.0.0.1      <none>        443/TCP        23h
letskube-service   LoadBalancer   10.0.68.173   <pending>     80:30582/TCP   26s
letskube-service   LoadBalancer   10.0.68.173   51.141.108.33   80:30582/TCP   58s
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> TerminatingError(): "The pipeline has been stopped."
>> TerminatingError(): "The pipeline has been stopped."
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get nodes
NAME                       STATUS    ROLES     AGE       VERSION
aks-nodepool1-13357558-0   Ready     agent     23h       v1.9.11
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> az aks scale --resource-group letskube-rg --name=letskubeakscluster --node-count 3
{
  "aadProfile": null,
  "addonProfiles": null,
  "agentPoolProfiles": [
    {
      "count": 3,
      "maxPods": 110,
      "name": "nodepool1",
      "osDiskSizeGb": 30,
      "osType": "Linux",
      "storageProfile": "ManagedDisks",
      "vmSize": "Standard_DS2_v2",
      "vnetSubnetId": null
    }
  ],
  "dnsPrefix": "letskubeak-letskube-rg-52e1c3",
  "enableRbac": true,
  "fqdn": "letskubeak-letskube-rg-52e1c3-f4bd6a76.hcp.ukwest.azmk8s.io",
  "id": "/subscriptions/52e1c3da-ce31-49d9-96ea-5c4a2a061a23/resourcegroups/letskube-rg/providers/Microsoft.ContainerSer
vice/managedClusters/letskubeakscluster",
  "kubernetesVersion": "1.9.11",
  "linuxProfile": {
    "adminUsername": "azureuser",
    "ssh": {
      "publicKeys": [
        {
          "keyData": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDXbv0YkyaTpZ6ss8fjQ7iL63cHk1Rwj+hWdytCygoFQEy2qTcTI7J/5Loxcl
RafbC8EBw51qzBaaqXd3GFkWoD/x44CG2LWwpmP6qREB1UXhlZuFAzqnK+lY9mP24+0hARUrlMay+e7dexFjdNZ9kf2InxXS1Z6Orqs/9EtMb5AB6bpYUkQQ
ajehteTHLTJJifR8bu4bJo6VHpjRbxce6/wMYwgA4YoI/qnj1bWZhpNZuhyyyaXheoK1XshqOBjBZqsRM3DgOdZwr4RhuxHDQJPEsFQ2PThGq7N7gSyVgU7m
n5dq1mdN2UpChWK0wuVgkuwVDi0bVuvgMy9UqtOKvz"
        }
      ]
    }
  },
  "location": "ukwest",
  "name": "letskubeakscluster",
  "networkProfile": {
    "dnsServiceIp": "10.0.0.10",
    "dockerBridgeCidr": "172.17.0.1/16",
    "networkPlugin": "kubenet",
    "networkPolicy": null,
    "podCidr": "10.244.0.0/16",
    "serviceCidr": "10.0.0.0/16"
  },
  "nodeResourceGroup": "MC_letskube-rg_letskubeakscluster_ukwest",
  "provisioningState": "Succeeded",
  "resourceGroup": "letskube-rg",
  "servicePrincipalProfile": {
    "clientId": "0ffb685e-a9d2-4c18-bf02-03fdf18d6d7f",
    "secret": null
  },
  "tags": null,
  "type": "Microsoft.ContainerService/ManagedClusters"
}
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get nodes
NAME                       STATUS    ROLES     AGE       VERSION
aks-nodepool1-13357558-0   Ready     agent     23h       v1.9.11
aks-nodepool1-13357558-1   Ready     agent     5m        v1.9.11
aks-nodepool1-13357558-2   Ready     agent     5m        v1.9.11
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS    RESTARTS   AGE
letskube-deployment-65ddc9bf5c-8n7wl   1/1       Running   0          14m
letskube-deployment-65ddc9bf5c-np9fs   1/1       Running   0          14m
letskube-deployment-65ddc9bf5c-tbfcl   1/1       Running   0          14m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl scale --replicas=5 deployment/letskube-deployment
deployment.extensions "letskube-deployment" scaled
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get deployment
NAME                  DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
letskube-deployment   5         5         5            3           14m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS              RESTARTS   AGE
letskube-deployment-65ddc9bf5c-8n7wl   1/1       Running             0          15m
letskube-deployment-65ddc9bf5c-d798r   0/1       ContainerCreating   0          25s
letskube-deployment-65ddc9bf5c-k4xrh   0/1       ContainerCreating   0          25s
letskube-deployment-65ddc9bf5c-np9fs   1/1       Running             0          15m
letskube-deployment-65ddc9bf5c-tbfcl   1/1       Running             0          15m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS    RESTARTS   AGE
letskube-deployment-65ddc9bf5c-8n7wl   1/1       Running   0          15m
letskube-deployment-65ddc9bf5c-d798r   1/1       Running   0          51s
letskube-deployment-65ddc9bf5c-k4xrh   1/1       Running   0          51s
letskube-deployment-65ddc9bf5c-np9fs   1/1       Running   0          15m
letskube-deployment-65ddc9bf5c-tbfcl   1/1       Running   0          15m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> docker build . -t letskubeacr.azurecr.io/letskube:v2
Sending build context to Docker daemon  1.784MB
Step 1/10 : FROM microsoft/aspnetcore-build AS build-env
 ---> 06a6525397c2
Step 2/10 : WORKDIR /app
 ---> Using cache
 ---> c8c4204049c5
Step 3/10 : COPY *.csproj ./
 ---> Using cache
 ---> 635b211f7989
Step 4/10 : RUN dotnet restore
 ---> Using cache
 ---> 934f9fabe219
Step 5/10 : COPY . ./
 ---> 0b0b083b822f
Step 6/10 : RUN dotnet publish -c Release -o output
 ---> Running in 5c29d59ea84a
Microsoft (R) Build Engine version 15.7.179.6572 for .NET Core
Copyright (C) Microsoft Corporation. All rights reserved.

  Restoring packages for /app/LetsKube.csproj...
  Generating MSBuild file /app/obj/LetsKube.csproj.nuget.g.props.
  Generating MSBuild file /app/obj/LetsKube.csproj.nuget.g.targets.
  Restore completed in 959.07 ms for /app/LetsKube.csproj.
  LetsKube -> /app/bin/Release/netcoreapp2.0/LetsKube.dll
  LetsKube -> /app/output/
Removing intermediate container 5c29d59ea84a
 ---> 114341871669
Step 7/10 : FROM microsoft/aspnetcore
 ---> db030c19e94b
Step 8/10 : WORKDIR /app
 ---> Using cache
 ---> 6a7a43923e17
Step 9/10 : COPY --from=build-env /app/output .
 ---> 3d875a5d38e8
Step 10/10 : ENTRYPOINT ["dotnet", "LetsKube.dll"]
 ---> Running in 34dc646cab9a
Removing intermediate container 34dc646cab9a
 ---> bb3c397498d0
Successfully built bb3c397498d0
Successfully tagged letskubeacr.azurecr.io/letskube:v2
SECURITY WARNING: You are building a Docker image from Windows against a non-Windows Docker host. All files and director
ies added to build context will have '-rwxr-xr-x' permissions. It is recommended to double check and reset permissions f
or sensitive files and directories.
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> docker push letskubeacr.azurecr.io/letskube:v2
The push refers to repository [letskubeacr.azurecr.io/letskube]
1e2f212c752c: Preparing
d7c758120571: Preparing
94a7e5001357: Preparing
fea4f503ccf8: Preparing
3172a1c8308a: Preparing
264a7fdea008: Waiting
3b10514a95be: Waiting
error parsing HTTP 403 response body: unexpected end of JSON input: ""
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> docker build . -t letskubeacrsp.azurecr.io/letskube:v2
Sending build context to Docker daemon  1.784MB
Step 1/10 : FROM microsoft/aspnetcore-build AS build-env
 ---> 06a6525397c2
Step 2/10 : WORKDIR /app
 ---> Using cache
 ---> c8c4204049c5
Step 3/10 : COPY *.csproj ./
 ---> Using cache
 ---> 635b211f7989
Step 4/10 : RUN dotnet restore
 ---> Using cache
 ---> 934f9fabe219
Step 5/10 : COPY . ./
 ---> Using cache
 ---> 0b0b083b822f
Step 6/10 : RUN dotnet publish -c Release -o output
 ---> Using cache
 ---> 114341871669
Step 7/10 : FROM microsoft/aspnetcore
 ---> db030c19e94b
Step 8/10 : WORKDIR /app
 ---> Using cache
 ---> 6a7a43923e17
Step 9/10 : COPY --from=build-env /app/output .
 ---> Using cache
 ---> 3d875a5d38e8
Step 10/10 : ENTRYPOINT ["dotnet", "LetsKube.dll"]
 ---> Using cache
 ---> bb3c397498d0
Successfully built bb3c397498d0
Successfully tagged letskubeacrsp.azurecr.io/letskube:v2
SECURITY WARNING: You are building a Docker image from Windows against a non-Windows Docker host. All files and director
ies added to build context will have '-rwxr-xr-x' permissions. It is recommended to double check and reset permissions f
or sensitive files and directories.
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> docker push letskubeacrsp.azurecr.io/letskube:v2
The push refers to repository [letskubeacrsp.azurecr.io/letskube]
1e2f212c752c: Preparing
d7c758120571: Preparing
94a7e5001357: Preparing
fea4f503ccf8: Preparing
3172a1c8308a: Preparing
264a7fdea008: Waiting
3b10514a95be: Waiting
unauthorized: authentication required
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> az login
Note, we have launched a browser for you to login. For old experience with device code, use "az login --use-device-code"

You have logged in. Now let us find all the subscriptions to which you have access...
[
  {
    "cloudName": "AzureCloud",
    "id": "e08bd715-5130-4e41-9416-2d575838d348",
    "isDefault": false,
    "name": "Pay-As-You-Go",
    "state": "Enabled",
    "tenantId": "2a1f458f-bb00-448e-a56b-32aa50056c02",
    "user": {
      "name": "steve@stevepettifer.com",
      "type": "user"
    }
  },
  {
    "cloudName": "AzureCloud",
    "id": "52e1c3da-ce31-49d9-96ea-5c4a2a061a23",
    "isDefault": true,
    "name": "personal-payg",
    "state": "Enabled",
    "tenantId": "b6b5cebd-21e2-4dbd-87d1-5443e3f5683b",
    "user": {
      "name": "steve@stevepettifer.com",
      "type": "user"
    }
  }
]
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> docker push letskubeacrsp.azurecr.io/letskube:v2
The push refers to repository [letskubeacrsp.azurecr.io/letskube]
1e2f212c752c: Preparing
d7c758120571: Preparing
94a7e5001357: Preparing
fea4f503ccf8: Preparing
3172a1c8308a: Preparing
264a7fdea008: Waiting
3b10514a95be: Waiting
unauthorized: authentication required
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS
NAMES
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> docker container ls
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS
NAMES
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> az acr login
az acr login: error: the following arguments are required: --name/-n
usage: az acr login [-h] [--verbose] [--debug]
                    [--output {json,jsonc,table,tsv,yaml,none}]
                    [--query JMESPATH] --name REGISTRY_NAME
                    [--resource-group RESOURCE_GROUP_NAME]
                    [--suffix TENANT_SUFFIX] [--username USERNAME]
                    [--password PASSWORD] [--subscription _SUBSCRIPTION]
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> az acr login --Name letskubeacrsp
az acr login: error: the following arguments are required: --name/-n
usage: az acr login [-h] [--verbose] [--debug]
                    [--output {json,jsonc,table,tsv,yaml,none}]
                    [--query JMESPATH] --name REGISTRY_NAME
                    [--resource-group RESOURCE_GROUP_NAME]
                    [--suffix TENANT_SUFFIX] [--username USERNAME]
                    [--password PASSWORD] [--subscription _SUBSCRIPTION]
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> az acr login -n letskubeacrsp
Login Succeeded
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> docker push letskubeacrsp.azurecr.io/letskube:v2
The push refers to repository [letskubeacrsp.azurecr.io/letskube]
1e2f212c752c: Pushed
d7c758120571: Layer already exists
94a7e5001357: Layer already exists
fea4f503ccf8: Layer already exists
3172a1c8308a: Layer already exists
264a7fdea008: Layer already exists
3b10514a95be: Layer already exists
v2: digest: sha256:885c7a4597f207519746ecf0dd2305a9977cfcb656a7bae44c6d8e528ccbdc03 size: 1790
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl set image deployment letskube-deployment letskube=letskubeacr.azurecr.io/letskube:v2
deployment.apps "letskube-deployment" image updated
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS         RESTARTS   AGE
letskube-deployment-5cdc97bc77-dk5rd   0/1       ErrImagePull   0          6s
letskube-deployment-5cdc97bc77-hxmt7   0/1       ErrImagePull   0          6s
letskube-deployment-5cdc97bc77-j4mc7   0/1       ErrImagePull   0          6s
letskube-deployment-65ddc9bf5c-8n7wl   1/1       Running        0          31m
letskube-deployment-65ddc9bf5c-d798r   1/1       Running        0          16m
letskube-deployment-65ddc9bf5c-np9fs   1/1       Running        0          31m
letskube-deployment-65ddc9bf5c-tbfcl   1/1       Running        0          31m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS             RESTARTS   AGE
letskube-deployment-5cdc97bc77-dk5rd   0/1       ImagePullBackOff   0          19s
letskube-deployment-5cdc97bc77-hxmt7   0/1       ImagePullBackOff   0          19s
letskube-deployment-5cdc97bc77-j4mc7   0/1       ImagePullBackOff   0          19s
letskube-deployment-65ddc9bf5c-8n7wl   1/1       Running            0          31m
letskube-deployment-65ddc9bf5c-d798r   1/1       Running            0          16m
letskube-deployment-65ddc9bf5c-np9fs   1/1       Running            0          31m
letskube-deployment-65ddc9bf5c-tbfcl   1/1       Running            0          31m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS         RESTARTS   AGE
letskube-deployment-5cdc97bc77-dk5rd   0/1       ErrImagePull   0          42s
letskube-deployment-5cdc97bc77-hxmt7   0/1       ErrImagePull   0          42s
letskube-deployment-5cdc97bc77-j4mc7   0/1       ErrImagePull   0          42s
letskube-deployment-65ddc9bf5c-8n7wl   1/1       Running        0          31m
letskube-deployment-65ddc9bf5c-d798r   1/1       Running        0          16m
letskube-deployment-65ddc9bf5c-np9fs   1/1       Running        0          31m
letskube-deployment-65ddc9bf5c-tbfcl   1/1       Running        0          31m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl set image deployment letskube-deployment letskube=letskubeacrsp.azurecr.io/letskube:v2
deployment.apps "letskube-deployment" image updated
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS              RESTARTS   AGE
letskube-deployment-5cdc97bc77-j4mc7   0/1       Terminating         0          1m
letskube-deployment-5cf68c987b-542xs   0/1       ContainerCreating   0          3s
letskube-deployment-5cf68c987b-jzkkg   0/1       ContainerCreating   0          3s
letskube-deployment-5cf68c987b-ll9bn   0/1       ContainerCreating   0          3s
letskube-deployment-65ddc9bf5c-8n7wl   1/1       Running             0          31m
letskube-deployment-65ddc9bf5c-d798r   1/1       Running             0          17m
letskube-deployment-65ddc9bf5c-np9fs   1/1       Running             0          31m
letskube-deployment-65ddc9bf5c-tbfcl   1/1       Running             0          31m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS              RESTARTS   AGE
letskube-deployment-5cdc97bc77-j4mc7   0/1       Terminating         0          1m
letskube-deployment-5cf68c987b-2g59q   0/1       ContainerCreating   0          2s
letskube-deployment-5cf68c987b-542xs   1/1       Running             0          6s
letskube-deployment-5cf68c987b-5fgq2   1/1       Running             0          3s
letskube-deployment-5cf68c987b-jzkkg   0/1       ContainerCreating   0          6s
letskube-deployment-5cf68c987b-ll9bn   1/1       Running             0          6s
letskube-deployment-65ddc9bf5c-8n7wl   0/1       Terminating         0          31m
letskube-deployment-65ddc9bf5c-d798r   0/1       Terminating         0          17m
letskube-deployment-65ddc9bf5c-np9fs   0/1       Terminating         0          31m
letskube-deployment-65ddc9bf5c-tbfcl   1/1       Running             0          31m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS              RESTARTS   AGE
letskube-deployment-5cf68c987b-2g59q   0/1       ContainerCreating   0          8s
letskube-deployment-5cf68c987b-542xs   1/1       Running             0          12s
letskube-deployment-5cf68c987b-5fgq2   1/1       Running             0          9s
letskube-deployment-5cf68c987b-jzkkg   0/1       ContainerCreating   0          12s
letskube-deployment-5cf68c987b-ll9bn   1/1       Running             0          12s
letskube-deployment-65ddc9bf5c-8n7wl   0/1       Terminating         0          32m
letskube-deployment-65ddc9bf5c-d798r   0/1       Terminating         0          17m
letskube-deployment-65ddc9bf5c-tbfcl   1/1       Running             0          32m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS              RESTARTS   AGE
letskube-deployment-5cf68c987b-2g59q   0/1       ContainerCreating   0          11s
letskube-deployment-5cf68c987b-542xs   1/1       Running             0          15s
letskube-deployment-5cf68c987b-5fgq2   1/1       Running             0          12s
letskube-deployment-5cf68c987b-jzkkg   0/1       ContainerCreating   0          15s
letskube-deployment-5cf68c987b-ll9bn   1/1       Running             0          15s
letskube-deployment-65ddc9bf5c-8n7wl   0/1       Terminating         0          32m
letskube-deployment-65ddc9bf5c-tbfcl   1/1       Running             0          32m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS              RESTARTS   AGE
letskube-deployment-5cf68c987b-2g59q   0/1       ContainerCreating   0          14s
letskube-deployment-5cf68c987b-542xs   1/1       Running             0          18s
letskube-deployment-5cf68c987b-5fgq2   1/1       Running             0          15s
letskube-deployment-5cf68c987b-jzkkg   0/1       ContainerCreating   0          18s
letskube-deployment-5cf68c987b-ll9bn   1/1       Running             0          18s
letskube-deployment-65ddc9bf5c-tbfcl   1/1       Running             0          32m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS              RESTARTS   AGE
letskube-deployment-5cf68c987b-2g59q   0/1       ContainerCreating   0          19s
letskube-deployment-5cf68c987b-542xs   1/1       Running             0          23s
letskube-deployment-5cf68c987b-5fgq2   1/1       Running             0          20s
letskube-deployment-5cf68c987b-jzkkg   0/1       ContainerCreating   0          23s
letskube-deployment-5cf68c987b-ll9bn   1/1       Running             0          23s
letskube-deployment-65ddc9bf5c-tbfcl   1/1       Running             0          32m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS              RESTARTS   AGE
letskube-deployment-5cf68c987b-2g59q   0/1       ContainerCreating   0          23s
letskube-deployment-5cf68c987b-542xs   1/1       Running             0          27s
letskube-deployment-5cf68c987b-5fgq2   1/1       Running             0          24s
letskube-deployment-5cf68c987b-jzkkg   0/1       ContainerCreating   0          27s
letskube-deployment-5cf68c987b-ll9bn   1/1       Running             0          27s
letskube-deployment-65ddc9bf5c-tbfcl   1/1       Running             0          32m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS              RESTARTS   AGE
letskube-deployment-5cf68c987b-2g59q   0/1       ContainerCreating   0          28s
letskube-deployment-5cf68c987b-542xs   1/1       Running             0          32s
letskube-deployment-5cf68c987b-5fgq2   1/1       Running             0          29s
letskube-deployment-5cf68c987b-jzkkg   0/1       ContainerCreating   0          32s
letskube-deployment-5cf68c987b-ll9bn   1/1       Running             0          32s
letskube-deployment-65ddc9bf5c-tbfcl   1/1       Running             0          32m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS        RESTARTS   AGE
letskube-deployment-5cf68c987b-2g59q   1/1       Running       0          32s
letskube-deployment-5cf68c987b-542xs   1/1       Running       0          36s
letskube-deployment-5cf68c987b-5fgq2   1/1       Running       0          33s
letskube-deployment-5cf68c987b-jzkkg   1/1       Running       0          36s
letskube-deployment-5cf68c987b-ll9bn   1/1       Running       0          36s
letskube-deployment-65ddc9bf5c-tbfcl   0/1       Terminating   0          32m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS        RESTARTS   AGE
letskube-deployment-5cf68c987b-2g59q   1/1       Running       0          35s
letskube-deployment-5cf68c987b-542xs   1/1       Running       0          39s
letskube-deployment-5cf68c987b-5fgq2   1/1       Running       0          36s
letskube-deployment-5cf68c987b-jzkkg   1/1       Running       0          39s
letskube-deployment-5cf68c987b-ll9bn   1/1       Running       0          39s
letskube-deployment-65ddc9bf5c-tbfcl   0/1       Terminating   0          32m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS        RESTARTS   AGE
letskube-deployment-5cf68c987b-2g59q   1/1       Running       0          40s
letskube-deployment-5cf68c987b-542xs   1/1       Running       0          44s
letskube-deployment-5cf68c987b-5fgq2   1/1       Running       0          41s
letskube-deployment-5cf68c987b-jzkkg   1/1       Running       0          44s
letskube-deployment-5cf68c987b-ll9bn   1/1       Running       0          44s
letskube-deployment-65ddc9bf5c-tbfcl   0/1       Terminating   0          32m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS        RESTARTS   AGE
letskube-deployment-5cf68c987b-2g59q   1/1       Running       0          41s
letskube-deployment-5cf68c987b-542xs   1/1       Running       0          45s
letskube-deployment-5cf68c987b-5fgq2   1/1       Running       0          42s
letskube-deployment-5cf68c987b-jzkkg   1/1       Running       0          45s
letskube-deployment-5cf68c987b-ll9bn   1/1       Running       0          45s
letskube-deployment-65ddc9bf5c-tbfcl   0/1       Terminating   0          32m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get pods
NAME                                   READY     STATUS    RESTARTS   AGE
letskube-deployment-5cf68c987b-2g59q   1/1       Running   0          44s
letskube-deployment-5cf68c987b-542xs   1/1       Running   0          48s
letskube-deployment-5cf68c987b-5fgq2   1/1       Running   0          45s
letskube-deployment-5cf68c987b-jzkkg   1/1       Running   0          48s
letskube-deployment-5cf68c987b-ll9bn   1/1       Running   0          48s
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> kubectl get service
NAME               TYPE           CLUSTER-IP    EXTERNAL-IP     PORT(S)        AGE
kubernetes         ClusterIP      10.0.0.1      <none>          443/TCP        1d
letskube-service   LoadBalancer   10.0.68.173   51.141.108.33   80:30582/TCP   32m
PS C:\_git\Pluralsight\Kubernetes Intro\letskube> stop-transcript
**********************
Windows PowerShell transcript end
End time: 20190307173447
**********************
